#include "PgmHandler.h"

PgmHandler::PgmHandler() : PgmHandler(1, 1, 255) {
}

PgmHandler::PgmHandler(unsigned int width, unsigned int height, unsigned int color_levels) : PnmBase(width, height, color_levels) {
  InitBuffer("P5", width, height, color_levels);
}

PgmHandler::~PgmHandler() {
}

bool PgmHandler::Read(const std::string& filename) {
  return false;
}

bool PgmHandler::Write(const std::string& filename) {
  return false;
}
