#include "ConvolutionFunc.h"

void do_convolve(const PgmHandler& in, PgmHandler& out, const unsigned int width, const unsigned int height, const unsigned int offset_x, const unsigned int offset_y, const float filter[9]) {
}

// Convolution filtering (not threaded)
void convolve_ST(const PgmHandler& in, PgmHandler& out, const float filter[9]) {
}


// Convolution filtering code (thread_count threads)
void convolve_MT(const PgmHandler& in, PgmHandler& out, const float filter[9], const unsigned int thread_count) {
}
