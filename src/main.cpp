#include <iostream>
#include <chrono>

#include "Config.h"
#include "ConvolutionFunc.h"

const float gaussian_blur_filter[]{  1.f / 16.f,   2.f/16.f,   1.f / 16.f,
                                     2.f / 16.f,   4.f/16.f,   2.f / 16.f,
                                     1.f / 16.f,   2.f/16.f,   1.f / 16.f };

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout << argv[0] << " version " << VERSION_MAJOR << "."
              << VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " image.pgm" << std::endl;
    return 1;
  }
  
  PgmHandler image;
  if(!image.Read(argv[1])) {
    std::cout << "Can't find image filename " << argv[1] << std::endl;
    return 1;
  }

  PgmHandler filtered_image(image.width(), image.height(), image.color_levels());
  auto start = std::chrono::high_resolution_clock::now();
  convolve_ST(image, filtered_image, gaussian_blur_filter);
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> elapsed = finish - start;
  std::cout << "convolve_ST elapsed time: " << elapsed.count() << " ms\n";
  filtered_image.Write("filtered.pgm");

  PgmHandler filtered_image_MT(image.width(), image.height(), image.color_levels());
  start = std::chrono::high_resolution_clock::now();
  convolve_MT(image, filtered_image_MT, gaussian_blur_filter, 4);
  finish = std::chrono::high_resolution_clock::now();
  elapsed = finish - start;
  std::cout << "convolve_MT elapsed time: " << elapsed.count() << " ms\n";
  filtered_image_MT.Write("filtered_MT.pgm");

  return 0;
}
