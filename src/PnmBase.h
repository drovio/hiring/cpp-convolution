#ifndef __PNMBASE_H__
#define __PNMBASE_H__

#include <string>
#include <fstream>
#include <sstream>

template <class T>
class PnmBase
{
public:
  PnmBase();
  PnmBase(unsigned int width, unsigned int height, unsigned int color_levels);
  virtual ~PnmBase();

  bool Set(unsigned int x, unsigned int y, const T& value);
  const T Get(unsigned int x, unsigned int y) const;
  
  virtual bool Read(const std::string& filename) = 0;
  virtual bool Write(const std::string& filename) = 0;
  
  inline const std::string& signature() const { return signature_; }
  inline unsigned int width() const { return width_; }
  inline unsigned int height() const { return height_; }
  inline unsigned int color_levels() const { return color_levels_; }
  inline const T* const_data() const { return buffer_; }

  void Clear(const T& value);

protected:
  bool InitBuffer(const std::string& signature, unsigned int width, unsigned int height, unsigned int color_levels);
  bool InitBufferFromFile(std::ifstream* file);
  
  inline T* data() const { return buffer_; }
  
private:
  std::string signature_;
  unsigned int width_;
  unsigned int height_;
  unsigned int color_levels_;
  T* buffer_;
};

#include "PnmBase.inl"

#endif
