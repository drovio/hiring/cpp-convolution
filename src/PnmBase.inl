template <class T>
PnmBase<T>::PnmBase() : PnmBase<T>(1, 1, 255) {
}

template <class T>
PnmBase<T>::PnmBase(unsigned int width, unsigned int height, unsigned int color_levels) :
    signature_("PN"),
    width_(width),
    height_(height),
    color_levels_(color_levels),
    buffer_(NULL) {
}

template <class T>
PnmBase<T>::~PnmBase() {
  if (buffer_ != NULL) {
    delete [] buffer_;
  }
}

template <class T>
bool PnmBase<T>::Set(unsigned int x, unsigned int y, const T& value) {
  if (buffer_ == NULL || width_ == 0 || height_ == 0) {
    return false;
  }
  
  if (x > width_ || y > height_) {
    return false;
  }
  
  *(buffer_ + y * width_ + x) = value;

  return true;
}

template <class T>
const T PnmBase<T>::Get(unsigned int x, unsigned int y) const {
  if (buffer_ == NULL || width_ == 0 || height_ == 0) {
    return T();
  }
  
  if (x > width_ || y > height_) {
    return T();
  }
  
  return *(buffer_ + y * width_ + x);
}

template <class T>
void PnmBase<T>::Clear(const T& value)
{
  if (buffer_ == NULL || width_ == 0 || height_ == 0) {
    return;
  }
  
  for (unsigned int y = 0; y < height_; y++) {
    for (unsigned int x = 0; x < width_; x++) {
      *(buffer_ + y * width_ + x) = value;
    }
  }
}

template <class T>
bool PnmBase<T>::InitBuffer(const std::string& signature, unsigned int width, unsigned int height, unsigned int color_levels) {
  if (buffer_ != NULL) {
    delete [] buffer_;
    buffer_ = NULL;
  }
  
  if (signature.empty() || signature.at(0) != 'P') {
    return false;
  }
  
  if(width == 0 || height == 0 || color_levels == 0) {
    return false;
  }
  
  signature_ = signature;
  width_ = width;
  height_ = height;
  color_levels_ = color_levels;
  
  buffer_ = new T[width_ * height_];
  
  return true;
}

template <class T>
bool PnmBase<T>::InitBufferFromFile(std::ifstream* file) {
  if (buffer_ != NULL) {
    delete [] buffer_;
    buffer_ = NULL;
  }
  
  // Retrieve the magic number
  char magic_number[3];
  
  file->read(magic_number, 2);
  file->ignore(1);
  
  magic_number[2] = '\0';
  
  if (magic_number[0] != 'P' ) return false;
  
  signature_ = magic_number;
  
  // Get width, height and color levels
  unsigned int frame_info[3];
  
  for (int i = 0; i < 3; i++) {
    std::string info;

    while(true) {
      char c;
      file->read(&c, 1);

      if (c != '\n' && c != ' ') {
        info += c;
      } else {
        break;
      }
    }

    std::istringstream iss(info);
    iss >> frame_info[i];
  }

  width_ = frame_info[0];
  height_ = frame_info[1];
  color_levels_ = frame_info[2];
  
  if(width_ == 0 || height_ == 0 || color_levels_ == 0) {
    return false;
  }
  
  buffer_ = new T[width_ * height_];

  return true;
}
