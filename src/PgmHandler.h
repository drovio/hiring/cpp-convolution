#ifndef __PGMHANDLER_H__
#define __PGMHANDLER_H__

#include "PnmBase.h"

class PgmHandler : public PnmBase<unsigned char> {
 public:
  PgmHandler();
  PgmHandler(unsigned int width, unsigned int height, unsigned int color_levels = 255);
  
  virtual ~PgmHandler();
  
  virtual bool Read(const std::string& filename);

  virtual bool Write(const std::string& filename);
};

#endif
