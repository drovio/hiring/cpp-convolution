#ifndef __CONVOLUTION_FUNC_H__
#define __CONVOLUTION_FUNC_H__

#include "PgmHandler.h"

// Convolution filtering code
// in: source image
// out: destination image
// width, height, offset_x and offset_y: source image area to consider
// filter: a 3x3 convolution kernel
void do_convolve(const PgmHandler& in, PgmHandler& out, const unsigned int width, const unsigned int height, const unsigned int offset_x, const unsigned int offset_y, const float filter[9]);

// Convolution filtering code (single-threaded, uses the do_convolve function)
// in: source image
// out: destination image
// filter: a 3x3 convolution kernel
void convolve_ST(const PgmHandler& in, PgmHandler& out, const float filter[9]);

// Convolution filtering code (multi-threaded, uses the do_convolve function)
// in: source image
// out: destination image
// filter: a 3x3 convolution kernel
// thread_count: number of threads
void convolve_MT(const PgmHandler& in, PgmHandler& out, const float filter[9], const unsigned int thread_count);

#endif
