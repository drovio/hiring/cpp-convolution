# cpp-convolution

This project has been designed to evaluate your skills when it comes to image handling, optimization and code structure in C++.

## Instructions

The instructions will be privately provided to you before the test starts.

## Requirements

For this test, you'll need a few tools installed on your machine:

* [cmake](https://cmake.org/download/) (3.10+)
* git
* Essential build tools or any IDE for C++ development
